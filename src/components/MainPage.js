import React, { Component } from 'react';
import Filter from './Filter/Filter';
import RecordTable from './RecordTable/RecordTable';

class MainPage extends Component {
    constructor() {
        super();
        this.state = {
            sortBy:""
        }
    }

    setSortBy = (sortBy) =>  {
        this.setState({ sortBy });
    }

    render() {
        return (
            <div>
                <center><h1>Birthday Records</h1></center>
                <Filter sortBy={this.state.sortBy} setSortBy={this.setSortBy} />
                <RecordTable sortBy={this.state.sortBy} />
            </div>
        );
    }
}

export default MainPage;