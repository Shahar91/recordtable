import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { people } from './Mock.json'
import Paper from '@material-ui/core/Paper';
import './style.css';

const convertSortByToSortFunction = {
    age:(a, b) => new Date(a.dob) - new Date(b.dob),
    name:(a, b) => a.name.localeCompare(b.name),
}

const RecordTable = (props) => {
    const { sortBy } = props;
    return (
        <Paper className="width">
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell className="table-header">Name</TableCell>
                        <TableCell className="table-header">Date of Birth</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody> 
                    {
                        people.sort(convertSortByToSortFunction[sortBy || "name"]).map((person) => (   
                                <TableRow key={person.name}>
                                    <TableCell className="table-cell">{person.name}</TableCell> 
                                    <TableCell className="table-cell" >{person.dob}</TableCell>
                                </TableRow>
                            )
                        )
                    }
                </TableBody>
            </Table>
        </Paper>
    );
}


export default RecordTable;
