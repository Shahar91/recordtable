import React, { Component } from 'react';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import './style.css'

class Filter extends Component {
    onChange = (sortBy) => {
        this.props.setSortBy(sortBy)
    }
    render() {
        return (
            <RadioGroup className="radio-boxes"  value={this.props.sortBy} onChange={(event) => this.onChange(event.target.value)}>
                <FormControlLabel className="radio-box" value="name" control={<Radio className="radio-box" />} label="Name" />
                <FormControlLabel className="radio-box" value="age" control={<Radio className="radio-box" />} label="Age" />
            </RadioGroup>
        )
    }
}

export default Filter;