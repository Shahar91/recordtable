import React from 'react';
import MainPage from './components/MainPage'

const App = () => {
    return (
      <div className="container-fluid">
        <MainPage />
      </div>
    );
}

export default App;
